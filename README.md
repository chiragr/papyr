![Papyr](papyr1.jpg)


## Getting Started 

Thank you for purchasing **Papyr**, the compact Nordic nRF52840 based epaper 
display developed by Electronut Labs.

Here's how you can get started with Papyr.

## Hardware Setup

Insert a CR2477 coin cell into the holder, or connect power via the micro USB 
connector. Make sure S2 is set to the correct position - USB or BATT.

The LED should start blinking GREEN. At this point, it's advertising 
as a BLE peripheral and you can connect to it from the Electronut Labs mobile 
app. 

## Using the Mobile App 

Download the Electronut Labs mobile app from the [Android Play store](https://play.google.com/store/apps/details?id=in.electronut.app). 
(The iOS version will be released soon.)

Click on *Papyr*, and you will should find your Papyr device in the list of 
scanned BLE peripherals. Click on the Papyr entry and you will see the following 
dialog. 

To start with, you can select the simplest option - drawing. 

![papyr app](app1.png)

As shown above, draw something on the screen, and hit "Send Image" when done. The 
LED on Papyr will blink BLE as the image is transferred, and in less than 30 
seconds, your image will appear on Papyr.

![papyr drawing](papyr2.jpg)

There are two other options for transferring images - selecting from your phone, 
and using your phone as an MQTT gateway. You can find a detailed guide to using 
these features at our [mobile app guide]().

Note that when not connected, Papyr will go to sleep to conserve power in about a 
minute. To wake it up, just press the S1 push button.

## Programming Papyr

Papyr can be reprogrammed using SWD header. You can use our inexpensive [Bumpy]()
SWD debugger for that purpose. You can read about the procedure [here](https://github.com/electronut/ElectronutLabs-Bumpy).

You can find other interesting applications of Papyr in the *code* directory.

If you need to revert to the default firmware, you can find the hex files under 
the top level *firmware* folder in this repository.

## Hardware Specifications

- Raytac MDBT50 module with Nordic nRF52840 BLE/802.15.4 SoC
- 1.54 inch 200x200 pixel red/black/white epaper display 
- CR2477 coin cell holder
- Micro USB (device) 
- RGB LED
- NFC (PCB) antenna
- Push button
- USB/Battery power switch
- Extra GPIOs
- SWD Programming header
- Mounting holes

## Papyr Dimensions

![papyr dims](papyr-dims.png)

## Current Consumption 

Papyr is designed for very low power applications. For the default fimrware that 
ships with Papyr, when the device is sleeping (not advertising), the current 
draw is about 6 uA. 

## Code Repository

You can find all code and design files related to Papyr at the link below:

[https://gitlab.com/electronutlabs-public/papyr/](https://gitlab.com/electronutlabs-public/papyr/)

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More 
information at our [website](https://electronut.in).





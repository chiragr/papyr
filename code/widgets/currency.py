# MQTT specifix library
import paho.mqtt.client as mqtt # Installed via: pip install paho-mqtt

# Rendering specific library
import svgwrite # Installed via: pip install svgwrite

# Data specific libraries
import json
import requests

# Broker and Topic Deta
MQTT_BROKER_HOST_ADDRESS = 'broker.hivemq.com'
MQTT_BROKER_HOST_PORT = 1883
MQTT_TOPIC = 'electronut/ZOHZL/svg' # IMP: Update this before running


def getData():
    # Get exchange rate in JSON format using REST API
    currencyUrl = 'http://free.currencyconverterapi.com/api/v5/convert?q=USD_INR'
    response = requests.get(currencyUrl)
    data = response.json()

    cnt = data['query']['count']

    res = data['results']
    usdINR = res['USD_INR']
    fr = str(cnt) + ' ' + usdINR['fr']
    to = usdINR['to']
    val = usdINR['val']

    return [fr, to, val]

def getSVGPayload():
    data = getData()

    # Create a drawing object
    dwg = svgwrite.Drawing('svgwrite-sample.svg', profile='tiny', viewBox=('0 0 200 200'))

    # Draw a black box
    dwg.add(dwg.rect((0, 0), (200, 200), fill='black'))

    # Draw text
    dwg.add(dwg.text(data[0],
        insert=(7, 90),
        fill='white',
        stroke_width=2,
        font_size='25px',
        font_weight="bold",
        font_family="Courier New")
    )

    # Draw text
    dwg.add(dwg.text(round(data[2], 2),
        insert=(5, 130),
        fill='white',
        stroke_width=2,
        font_size='50px',
        font_weight="bold",
        font_family="Courier New")
    )

    # Draw text
    dwg.add(dwg.text(data[1],
        insert=(155, 130),
        fill='white',
        stroke_width=2,
        font_size='22px',
        font_weight="bold",
        font_family="Courier New")
    )

    # String will be sent as payload
    return dwg.tostring()


# Create client and connect to it
client = mqtt.Client()
client.connect(MQTT_BROKER_HOST_ADDRESS, MQTT_BROKER_HOST_PORT)

# Generate payload in SVG format
payload = getSVGPayload()

# Publish data from broker to clients
client.publish(MQTT_TOPIC, payload)
